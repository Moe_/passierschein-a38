from cocos.sprite import Sprite
from Counter import *
from MovePlayerLogic import TileModel
from MovePlayerLogic import TileElement

class Tile:
    TYPE_COUNT = 12
    TYLE_OFFSET = 3
    def __init__(self, type, scene):
        self.type = type
        self.loadGraphics()
        scene.add(self.sprite)
        self.counters = []

    def loadGraphics(self):
        if self.type == 0:
            self.sprite = Sprite( 'graphics/ceiling.png' )
        elif self.type == 1:
            self.sprite = Sprite( 'graphics/corner1.png' )
        elif self.type == 2:
            self.sprite = Sprite( 'graphics/corner2.png' )
        elif self.type == 3:
            self.sprite = Sprite( 'graphics/corner3.png' )
        elif self.type == 4:
            self.sprite = Sprite( 'graphics/corner4.png' )
        elif self.type == 5:
            self.sprite = Sprite( 'graphics/empty.png' )
        elif self.type == 6:
            self.sprite = Sprite( 'graphics/floor-and-ceiling.png' )
        elif self.type == 7:
            self.sprite = Sprite( 'graphics/floor.png' )
        elif self.type == 8:
            self.sprite = Sprite( 'graphics/stairs1.png' )
        elif self.type == 9:
            self.sprite = Sprite( 'graphics/stairs2.png' )
        elif self.type == 10:
            self.sprite = Sprite( 'graphics/uform1.png' )
        elif self.type == 11:
            self.sprite = Sprite( 'graphics/uform2.png' )
        else: #some stupid request
            self.sprite = Sprite( 'graphics/ceiling.png' )
            self.type = 0

    def loadData(self, house, player):
        if self.type == 0:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 1:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 2:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 3:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 4:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 5:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 6:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 7:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 8:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 9:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 10:
            self.model = TileModel(TileElement.FLOOR, house, player)
        elif self.type == 11:
            self.model = TileModel(TileElement.FLOOR, house, player)

    def setPosition(self, x, y):
        self.sprite.position = (x, y)
        
    def getSize(self):
        return self.sprite.get_rect()

    def getType(self):
        return self.type

    def hasSpace(self):
        size = len(self.counters)
        if (self.type == 8 or self.type == 9) and size == 1:
            return False
        elif size == 2:
            return False
        return True
            
    def addCounter(self, id):
        if not self.hasSpace():
            return False
        if self.type == 8 or self.type == 9:
            self.counters.append(Counter(id, self.sprite, 0, self.getSize().width + Tile.TYLE_OFFSET, self.type))
        elif len(self.counters) == 0:
            self.counters.append(Counter(id, self.sprite, 0, self.getSize().width + Tile.TYLE_OFFSET, self.type))
        else:
            self.counters[0].relocate(-1 * (self.getSize().width + Tile.TYLE_OFFSET) / 4, self.getSize().width + Tile.TYLE_OFFSET)
            self.counters.append(Counter(id, self.sprite, (self.getSize().width + Tile.TYLE_OFFSET) / 4, self.getSize().width + Tile.TYLE_OFFSET, self.type))
        return True

    def getCounters(self):
        return self.counters