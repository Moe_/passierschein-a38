from cocos.sprite import Sprite
from constants import *
from cocos.actions import Delay
from cocos.actions import CallFuncS
import time
from threading import Timer

class Player:
    
    STEP_SIZE = 0.6
    MAX_STEP_SIZE = 0.63
    carrier = 0
    positionInTile = 0.5
    
    def __init__(self, scene, tile_row, tile_column, x, y, tile_width, tile_height):
        self.scene = scene
        self.loadSprites()
        self.sprite = self.spritesFront[0]
        scene.add(self.sprite, 3)
        self.x = x
        self.y = y
        self.tile_column = tile_column
        self.tile_row = tile_row
        self.tile_width = tile_width
        self.tile_height = tile_height
        self.isUpsideDown = False
        self.mood = 0 # mood is better the higher this value is, 0 is the best mood, 3 the worst
        self.direction = -1 # 0 no movement, -1 to the left, +1 to the right
        self.currentAnimationStep = 0
        self.exitThread = False
        Timer(CHAR_ANIM_INTERVAL, self.updateSprite, ()).start()

    def loadSprites(self):
        self.spriteNamesLeft = ['char_go_left1.png', 'char_go_left2.png', 'char_go_left3.png']
        self.spriteNamesRight = ['char_go_right1.png', 'char_go_right2.png', 'char_go_right3.png']
        self.playerNamesFront = ['char3.png', 'char1.png', 'char4.png', 'char2.png']
        self.spritesLeft = []
        self.spritesRight = []
        self.spritesFront = []
        for spriteName in self.spriteNamesLeft:
            spriteName = CHAR_DIRECTORY + spriteName
            sprite = Sprite(spriteName)
            self.scene.add(sprite, 3)
            self.spritesLeft.append(sprite)
        for spriteName in self.spriteNamesRight:
            spriteName = CHAR_DIRECTORY + spriteName
            sprite = Sprite(spriteName)
            self.scene.add(sprite, 3)
            self.spritesRight.append(sprite)
        for spriteName in self.playerNamesFront:
            spriteName = CHAR_DIRECTORY + spriteName
            sprite = Sprite(spriteName)
            self.scene.add(sprite, 3)
            self.spritesFront.append(sprite)

    def move(self, dt, dx, dy):
        self.x = self.x + dx * dt * 25
        self.y = self.y + dy * dt * 25
        
        #remove later when logic is done
        while self.x > self.tile_width:
            self.tile_column = self.tile_column + 1
            self.x = self.x - self.tile_width
        while self.x < 0:
            self.tile_column = self.tile_column - 1
            self.x = self.x + self.tile_width
        
        while self.y > self.tile_height:
            self.tile_row = self.tile_row + 1
            self.y = self.y - self.tile_height
        while self.y < 0:
            self.tile_row = self.tile_row - 1
            self.y  = self.y + self.tile_height
        
        x = self.tile_column * self.tile_width + self.x
        y = self.tile_row * self.tile_height + self.y
        
        self.sprite.position = (x, y)
        
        if dx == 0:
            if dx != self.direction:
                self.currentAnimationStep = 0
                self.direction = 0
        elif round(dx/abs(dx)) != self.direction:
            self.currentAnimationStep = 0
            self.direction = round(dx/abs(dx))
    
    def changeAnimationInDirection(self):
        #stop all actions, if there are still animations running
        self.sprite.stop()
        self.currentAnimationStep = 0
        if (self.direction == 0):
            self.sprite.visible = False
            self.sprite = spritesFront[self.mood]
        else:
            animDelay = Delay(CHAR_ANIM_INTERVAL)
            func = CallFuncS(assignNextSpriteAnimation)
            animAction = animDelay + func
            self.sprite.do(animAction, self)
    
    def getFullPosition(self):
        return self.tile_column, self.tile_row, self.x, self.y

    def getSize(self):
        return self.spritesLeft[0].get_rect()

    def updateSprite(self):
        self.currentAnimationStep += 1
        animationSprites = []
        if (self.direction == 0):
            animationSprites = self.spritesFront
        if (self.direction == -1 and self.isUpsideDown == False) or (self.direction == 1 and self.isUpsideDown == True):
            animationSprites = self.spritesLeft
        if (self.direction == 1 and self.isUpsideDown == False) or (self.direction == -1 and self.isUpsideDown == True):
            animationSprites = self.spritesRight
        self.currentAnimationStep = self.currentAnimationStep % len(animationSprites)
        newSprite = self.sprite
        if self.direction == -1 or self.direction == 1:
            newSprite = animationSprites[self.currentAnimationStep]
        elif self.direction == 0:
            newSprite = animationSprites[self.mood]
        newSprite.position = self.sprite.position
        newSprite.anchor = 0.5, 0.0
        self.sprite.visible = False
        self.sprite = newSprite
        self.sprite.visible = True
        if self.isUpsideDown:
            self.sprite.rotation = 180
        else:
            self.sprite.rotation = 0
        if self.exitThread == False:
            Timer(CHAR_ANIM_INTERVAL, self.updateSprite, ()).start()
