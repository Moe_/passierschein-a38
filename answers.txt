### Pfoertner
1 Bitte was?
2 Ich pfeif auf ihre Galeeren! Ich will doch nur den Passierschein A38!
Wo haengt der Plan von diesem Gebaeude?

### Hilde
1 Dafuq, warum?
2 Den hab ich seit nem halben Jahr nicht mehr gesehen.
2 Auf Wiedersehen
2 Wirklich? Ich dachte, der waere nur ein windiger Bauunternehmer.
1 Gibt es bei Ihnen das gruene Formular?
2 Was denn fuer ein rotes Formular?

### Zapp Brannigan
1 Ich haette gerne den Passierschein A38
1 Ne, Leela hab ich nicht gesehen. Gibs auf Zapp, du kriegst sie eh nicht!
2 Nein, aber ich habe das gruene Formular
3 Ach, Sie koennen mich mal!
3 Tja, n blaues Formular hab ich nicht.
3 Meinetwegen. Wo kann ich es denn bekommen? 
1 Kann man hier Drogen kaufen?

### Frieda
1 Ich haette gerne den Passierschein A 38
1 Wissen Sie, wo ich das blaue Formular kriege?
1 Wo bekomme ich das gruene Formular?
1 Wo bekomme ich das gelbe Formular?
1 Gibt es ueberhaupt das lilablassblaue Formular?
1 Nein, ich habe keine Ahnung
1 Versuchen Sie es mal im ersten Stock
1 DEN PASSIERSCHEIN A ACHTUNDREISSIG!!!!
2 Ja, ich wuesste gerne, wo ich das lila Formular bekomme
2 Nein
2 Auf Wiedersehen
2 Wollen wir uns nicht mal auf nen Kaffee treffen?

### Der fette Hobbit
1 Dafuq?
1 Wie bitte? Ich hab doch grade erst das blaue, das rote, das gelbe, das lilablassblaue,
das gruene, das ultraviolette, das infrarote und das kotzgruene Formular
von Pontius nach Pilatus getragen! Ich will diesen Passierschein und zwar jetzt!
1 Irrtum. Diese Weisung ist gemaess dem Zentralen Memo fuer parasemiotische Studien aufgehoben.
1 Kann man das essen?
2 Hier bitte
2 *ruelps*
2 Ach jetzt reichts mir, ich geh jetzt Drogen kaufen
2 Ich will hier raus
2 Wo bekomme ich das infrarote Formular

### Der Anwaltsdaemon
1 Das hab ich nicht
1 Nein und nein
1 Hier ist es
1 Schuldig im Sinne der Anklage
1 Dafuq?
1 Aber immer!
2 Nee-heein!
2 NEEEEEEEIIIIIINNNN
2 Meinetwegen
2 Ja
2 Juhu! Endlich ein neues Formular!
2 Meine Seele gegen ein Formular? Das klingt fair.

### Der andere Anwaltsdaemon
1 Ich moechte das gruene Formular
2 Nein, das waer dann alles. Vielen Dank
1 Morgen. Ich brauche das gelbe Formular
2 Danke
3 Wiedersehen!
3 ZURUECK IN DIE HOELLE, DAEMONEN-ABSCHAUM!

### Lucius
1 Salve! Hier ist das gruene Formular. Was kriege ich dafuer?
2 Danke sehr. Irgendwer hat mir gesagt ich brauche auch noch das rosa Formular. Wo finde ich das?
3 Ach wirklich?
3 Wollen Sie mich rollen?
3 Zu anstrengend. Ich will Kaffee!
3 Danke sehr
4 Ach, darauf bin ich noch nie gekommen
4 uebrigens, der Kommunismus hat noch nie funktioniert.
4 Das glaub ich Ihnen gerne
4 Ebenfalls einen schoenen tag noch.
4 Tschuessikowski!

### Mucius
1 Nee, keine Ahnung
1 Weiss ich nicht
1 Sicher nicht. Und das Wetter war auch schon mal besser.
1 Ja, hier ist es
1 Nein, aber ich hab noch ein Bier uebgig. Willst eines?
1 Das Wetter ist ganz toll. Haben Sie das rosa Formular?
2 Wie bitte?
2 Um Klarstellung wird gebeten?
2 Scheren Sie sich zum Teufel, Muciusissowitsch!
2 Juhuuuu!
2 Na toll!
2 Das soll ja wohl ein Witz sein!
3 Auf Wiedersehen
3 *rennt weg*
3 Entschuldigen Sie bitte die Stoerung
3 Danke sehr!
2 Was hat das denn damit zu tun?
3 Was soll diese Frage denn jetzt?
3 Versteh ich nicht.

### Der Doktor
1 Scheren Sie sich zum Teufel, Doktor.
1 Jaaaa! Ich komm hier nicht mehr raus! Die sind doch alle irre!
1 Verkaufen Sie Drogen?
1 Ich moechte den Passierschein A 38
1 Weiss hier eigentlich irgendjemand, welche formalen Voraussetzungen der Passierschein A 38 hat?
2 Aber Sie koennen mir doch garantiert helfen. Sehen Sie sich an, Sie sind Arzt! Die Goetter in Weiss!
2 Also jetzt mal unter uns Pfarrerstoechtern: Der Pfoertner hat doch nen Knall. Staendig will er mich zum Hafen schicken! Versteht der nicht was ich von ihm will?
2 Wie, den Passierschein A 38 gibt es nicht. Ist es das, was Sie damit sagen wollen? 
2 Aber sicher gibt es den Passierschein A 38. Warum sollte es ihn nicht geben.
2 Doktor, ich habe meine Meinung geaendert. Ich moechte gerne das infrarote Formular.
2 Anderes Thema: wo finde ich die Kantine?
2 Andere Frage: Wie komme ich zu Schalter 8?
3 Das war nicht meine Frage, Sie Idiot!
3 Das wollte ich nicht wissen.
3 Fragen sie das den Anwaltsdaemon.
3 Danke sehr! Ich versuch, mich weiter zurecht zu finden.
3 Klar, warum nicht?
3 Muss ich jetzt weiter zum Ende des Ganges?
4 Tatsaechlich?
4 Tatsaechlich? Das haette ich nicht gedacht.
4 Was wohl Frau Frieda dazu sagt. Sie schien nicht gerade freundlich beim letzten Mal.

### Der Praktikant
1 Nein
1 Rauchen ist ungesund
1 Klar, hier bitte. Weisst du, wer hier die Aufsicht hat?
2 Achso
2 Also. Leuten wie dir gibt man oefter die Schuld, oder?
2 Das ist keine Entschuldigung.
3 Auf Wiedersehen
3 Tschuss
3 Hier is Feuer. *zigarette anzuend*
3 *demonstrativ eigene Zigarette ansteckt und anzuend*

### Sophisticus
1 Das koennen wir einfach nicht wissen
1 Real ist, was wir wahrnehmen
1 Laut Wittgenstein ist die Welt das, was der Fall ist.
1 Aber vielleicht gibt es doch einen Weg, diese Frage zu entscheiden.
2 Und gewinnt oder verliert unser Leben vielleicht an Wert auf Grundlage solchen Solipzismus?
2 Selbst wenn es so waere: was koennen wir tun, um es zu entscheiden? Eine clevere Maschine konstruieren oder ein ausgekluegeltes Experiment?
2 Jedenfalls zeigt Wittgenstein auf, das wir nun mal nicht hinter die Kulissen gucken koennen. Wir koennen die Simulation nicht verlassen. Aber koennen wir sie durch gezieltes Denken sozusagen provozieren?
2 Fakt ist, wir muessen mit dem vorliebnehmen, was uns zur Verfuegung steht. Wenn nur die externe Welt eine Simulation ist, dann koennen unsere Gedanken frei sein.
3 Aber welchen Zweck sollte es haben, die Simulation durch gezieltes Nachdenken ueber sie zu provozieren? Koennte sie das nicht einfach durchschauen? Oder einfach ignorieren?
3 Wenn unsere Gedanken auch Teil der Simulation sind, dann ist es doch erst recht muessig darueber nachzudenken, ob man Einfluss nehmen kann. Dann kann man sogar unsere Freiheit prinzipiell hinterfragen.
3 Ich glaube nicht, das unsere Gedanken Teil der Simulation sind. Beim Gehirn im Tank ist ja auch die Neuralprothese die Grenze und Schnittstelle der Computermaschinerie.
3 Wenn einer von uns stirbt, dann zeigt das, das die Simulation mitliest und unsere Gedanken nicht frei sind. Das wir sowas ausessen koennen zeigt entweder das unsere Gedanken frei sind oder das die Simulation diesen Fall vorgesehen hat und es sie nicht stoert.
4 Wuerde das etwas aendern?
4 Waere das nicht in irgendeiner Weise vom Simulator vorgesehen und waere unser Leben tatsaechlich durch diesen Solipzismus bedroht, dann waren wir jetzt entweder tot oder verrueckt.

### Der Nachtwaechter
1 Melosine
1 Leben oder sterben
1 Bruegge is n Scheisskaff
1 FUER SPARTA!
1 Ich habs vergessen
1 Zeta Tau Phi!
1 Swordfish
2 Yay?
2 Juhu!
2 Neeeeeeiiiiinnn!
2 Oops

### Der robotische Roboterautomatenschalterroboter
1 EinsNullEinsNull
1 NullEinsEinsEinsEinsNullEinsNull
2 NullEinsEinsNullEinsEinsNull
2 EinsNullNullNullNullEinsEinsEins
2 EinsEinsNull
2 EinsNullNullNullEins
2 NullEinsEinsEins
3 EinsEinsEinsEins
3 NullNullNullNull
3 EinsNullNullNullEinsEinsEinsEinsNull
3 EinsNullEins
4 NullNullNullNullNull
4 NullEinsEinsEInsNull
4 EinsNullNullNullEinsEinsEinsEins
4 NullNullNullEinsEins .. . ZWEI!

### Tante Erna
1 Ja bitte!
1 Nein danke. Ich frage mich eher, wo ich das infrarote Formular herbekomme.
2 Danke sehr!
2 Dankeschoen.
2 Wo muss ich unterschreiben?
2 Nein, ich brauche das gelbe Formular.
2 Tut mir leid, eigentlich brauche ich das gruene Formular.
3 Was? Das wollte ich doch gar nicht!
3 Dankeschoen
3 vielen Dank
3 Danke. Haben Sie auch das andere noch?
3 Nein, das gelbe tuts auch.

### Ein Getraenkeautomat
1 Bier
1 Kaffee
1 Wasser
1 Coca Cola
1 Pepsi
1 Sprite
1 Absinth
1 Motoroel 
1 Das blaue Formular
2 Ein Euro
2 Fuenfzig Cent
2 Fuenfundvierzig Cent
2 Zwei Euro
2 Drei Euro
