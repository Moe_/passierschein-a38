import cocos
from cocos.director import director
from cocos.actions import *
from cocos.layer import *
from cocos.sprite import Sprite
from cocos.text import Label
import pyglet
from constants import *
import pyglet

from Dialogs import *
from House import *

class DialogScene(cocos.layer.Layer):
    is_event_handler = True

    def __init__(self, employee_name, counter_number, house, theMood):
        super( DialogScene, self ).__init__()	
	    
        self.house = house

        background = Sprite('graphics/counter-fullscreen.png')
        background.position = GAME_RES_WIDTH / 2, GAME_RES_HEIGHT / 2
        self.add( background, 0 )
        employee = Sprite('graphics/employee1.png');
        employee_width, employee_height = employee.get_rect().size
        employee.position = GAME_RES_WIDTH / 2, 320 + employee_height / 2
        self.add( employee, 1 )
        
        self.dialogFace = ["char3_dialog.png", "char1_dialog.png", "char4_dialog.png", "char2_dialog.png"]
        playerImg = self.set_player_sprite_from_mood(theMood)
        
    	name_label = cocos.text.Label(employee_name,
            font_name='Verdana',
            font_size=20,
            anchor_x='center', anchor_y='center', color = (0,0,0,255))
        name_label.position = GAME_RES_WIDTH / 2, 245
        self.add( name_label, 2 )

        counter_label = cocos.text.Label(counter_number,
            font_name='Verdana',
            font_size=40,
            anchor_x='center', anchor_y='center', color = (0,0,0,255))
        counter_label.position = GAME_RES_WIDTH / 2, 630
        self.add( counter_label, 2 )

        self.dialog = Dialog(employee_name)
        self.set_dialog_labels()

        self.keys_pressed = set()
        
        #textplocks = ['alle meine gruenchen', 'wtf, kann man das essen?', 'wie findest du blau', 'so rosarote brillen sind echt doof', 'die formulare sind egal', 'ich bin so gelb vor neid']
        #for s in textplocks:
        #   print(s, self.parse_dialog_for_color_direction(s))
    def set_player_sprite_from_mood(self, mood):
        playerFace = Sprite(CHAR_DIRECTORY+self.dialogFace[mood])
        playerFaceWidth, playerFaceHeight = playerFace.get_rect().size
        background = Sprite('graphics/counter-fullscreen.png')
        backgroundWidth, backgroundHeight = background.get_rect().size
        blackborderWidth = (GAME_RES_WIDTH - backgroundWidth) / 2
        playerFace.position = blackborderWidth + playerFaceWidth / 2, playerFaceHeight /2
        #playerFace.position = 200, 200
        #print playerFaceWidth, " ", playerFaceHeight
        self.add(playerFace, 1)

    def set_dialog_labels(self):
        self.clean_dialog_labels()
        question = self.dialog.question
        answers = self.dialog.answerOptions

        space_per_dialog_option = MAX_DIALOG_SPACE / 5

        self.dialog_labels.extend(self.split_draw_dialog_label(question, (GAME_RES_WIDTH + 180) / 2, space_per_dialog_option * 5.5))
        i = 0
        for answer in answers:
            split_dialog_labels = self.split_draw_dialog_label(str(i + 1) + answer, (GAME_RES_WIDTH + 180) / 2, space_per_dialog_option * (4.5 - i))
            self.dialog_labels.extend(split_dialog_labels)
            i += 1

    def clean_dialog_labels(self):
        if hasattr(self, 'dialog_labels'):
            for dialog_label in self.dialog_labels:
                dialog_label.kill()
        self.dialog_labels = []

    def split_draw_dialog_label(self, text, x, y):
        space_per_dialog_option = MAX_DIALOG_SPACE / 5 / 4
        labels = []
        mid_offset = - (MAX_SPLIT_ALLOWED - 1) / 2
        i = 0
        for s in text.split('\n'):
            labels.append(self.draw_dialog_label(s, x, y - (mid_offset + i) * space_per_dialog_option))
            i += 1
        return labels
        
    def draw_dialog_label(self, text, x, y):
        label = cocos.text.Label(text,
            font_name='Verdana',
            font_size=10,
            anchor_x='center', anchor_y='center', color = (0,0,0,255))
        label.position = x, y
        self.add( label, 2)
        return label

    #def on_key_press (self, key, modifiers):
	

    def on_key_release (self, key, modifiers):
        if key == pyglet.window.key.NUM_1 or key == pyglet.window.key.NUM_2 or key == pyglet.window.key.NUM_3 or key == pyglet.window.key.NUM_4 or key == pyglet.window.key._1 or key == pyglet.window.key._2 or key == pyglet.window.key._3 or key == pyglet.window.key._4:
            if (key == pyglet.window.key.NUM_1 or key == pyglet.window.key._1) and len(self.dialog.answerOptions) >= 1:
                color = self.parse_dialog_for_color_direction(self.dialog.answerOptions[0])
                self.house.shiftByColor(color)
            if key == pyglet.window.key.NUM_2 or key == pyglet.window.key._2 and len(self.dialog.answerOptions) >= 2:
                color = self.parse_dialog_for_color_direction(self.dialog.answerOptions[1])
                self.house.shiftByColor(color)
            if key == pyglet.window.key.NUM_3 or key == pyglet.window.key._3 and len(self.dialog.answerOptions) >= 3:
                color = self.parse_dialog_for_color_direction(self.dialog.answerOptions[2])
                self.house.shiftByColor(color)
            if key == pyglet.window.key.NUM_4 or key == pyglet.window.key._4 and len(self.dialog.answerOptions) >= 4:
                color = self.parse_dialog_for_color_direction(self.dialog.answerOptions[3])
                self.house.shiftByColor(color)
            if (self.dialog.maximumStage == self.dialog.stage):
               cocos.director.director.pop()
            self.dialog.incStageAndNextQuestion()
            self.set_dialog_labels()

    def parse_dialog_for_color_direction(self, s):
        for color in DIRECTION_COLORS:
            if s.find(color) != -1:
                return color
        

if __name__ == "__main__":
    mood = 1
    cocos.director.director.init(GAME_RES_WIDTH, GAME_RES_HEIGHT, "Passierschein A38")
    dialog_layer = DialogScene('Der fette Hobbit', 'XI', None, mood)
    start_scene = cocos.scene.Scene(Layer());
    #cocos.director.director.run(start_scene)
    main_scene = cocos.scene.Scene(dialog_layer)
    cocos.director.director.run(main_scene)


		
