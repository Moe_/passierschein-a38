import cocos
from cocos.director import director
from cocos.actions import *
from cocos.layer import *
from cocos.sprite import Sprite
import cocos.audio.music
import cocos.audio.effect
import pyglet

from House import *

class GameScene(cocos.layer.Layer):
    is_event_handler = True
    xDir = 0
    yDir = 0

    def __init__(self):
        super( GameScene, self ).__init__()
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        l = ColorLayer(255, 255, 255, 255)
        self.add(l)
        self.house = House(self, 5, 6)
        self.keys_pressed = set()
        self.schedule(self.updateTasks, None, None)
        
        self.mixer = cocos.audio.pygame.mixer
        self.mixer.init()
        self.mc = cocos.audio.music.MusicControl()
        self.mc.load("sounds/a38_theme_master.ogg")
        self.mc.play()
        #self.walk = cocos.audio.effect.Effect("sounds/A38_step.ogg")
        #self.walk.play()

    def on_key_press(self, key, modifiers):
        if key == pyglet.window.key.F:
            self.house.cycleRowRight(3)
        if key == pyglet.window.key.G:
            self.house.cycleRowLeft(3)
        if key == pyglet.window.key.V:
            self.house.cycleColumnUp(3)
        if key == pyglet.window.key.B:
            self.house.cycleColumnDown(3)
        
        if key == pyglet.window.key.LEFT or key == pyglet.window.key.A:
            self.xDir = -1
        if key == pyglet.window.key.RIGHT or key == pyglet.window.key.D:
            self.xDir = 1
        if key == pyglet.window.key.UP or key == pyglet.window.key.W:
            self.yDir = 1
        if key == pyglet.window.key.DOWN or key == pyglet.window.key.S:
            self.yDir = -1
        if key == pyglet.window.key.ENTER or key == pyglet.window.key.SPACE:
            self.house.checkCounter()

    def on_key_release(self, key, modifiers):
        if key == pyglet.window.key.LEFT or key == pyglet.window.key.RIGHT or key == pyglet.window.key.A or key == pyglet.window.key.D:
            self.xDir = 0
        if key == pyglet.window.key.UP or key == pyglet.window.key.DOWN or key == pyglet.window.key.W or key == pyglet.window.key.S:
            self.yDir = 0

    def on_exit(self):
        self.house.player.exitThread = True

    def updateTasks(self, dt, args, kargs):
        #if self.xDir != 0 or self.yDir != 0:
        #    self.walk = cocos.audio.effect.Effect('sounds/A38_step.ogg')
        #    self.walk.play()
        self.house.updatePlayer(dt, self.xDir, self.yDir)
