
from House import *
from Player import *
from Tile import *

#public - all of it.
class TileElement:
    WALL_LEFT = 1
    CEILING = 2
    WALL_RIGHT = 4
    FLOOR = 8
    STAIRS_BL = 16 # stairs from bottom left to top right corner
    STAIRS_BR = 32 # stairs from bottom right to top left corner
    STAIRS = STAIRS_BL | STAIRS_BR


class TileModel:
    # the threshhold at which the player is considered leaving the tile (next move makes him switch carrier)
    LEAVE_THRESHHOLD = Player.MAX_STEP_SIZE * 1.25 # must be bigger than the steps the guy makes, so use factor > 1
    
    # unique tile identifier
    id = 0;
    
    # the composition of the element
    composition = 0;
    
    # the house, containing the tiles
    house = 0
    
    # the player with its positions and stuff
    player = 0
    
    # Handler for da moves
    moveHandler = 0

    cockBlockComposition = TileElement.CEILING | TileElement.FLOOR
    
    
    # public
    def __init__(self, composition, house, player):
        self.composition = composition;
        self.player = player
        player.carrier = TileElement.FLOOR
        self.house = house
        self.moveHandler = MoveHandler(self)        
    
    def getTile(self, x, y):
        return self.house.TileArray[y][x].model
    
    def getNeighbouredTile(self, offsetX, offsetY):
        x = self.player.tile_column
        y = self.player.tile_row
        x = x + offsetX
        x = (x + self.house.columns) % self.house.columns
        y = y - offsetY
        if (y < 0 or y >= self.house.rows):
            return TileModel(self.cockBlockComposition, self.house, self.player)
        return self.getTile(x, y)
        
        
    def hasTileElement(self, tileElement):
        return (self.composition & tileElement) > 0
        
    # whether the tile has a blocking wall on the left side, respecting the tile on the left
    def hasLeftWall(self):
        return self.hasTileElement(TileElement.WALL_LEFT) or self.getNeighbouredTile(-1, 0).hasTileElement(TileElement.WALL_RIGHT)

    # whether the tile has a blocking wall on the right side, respecting the tile on the right
    def hasRightWall(self):
        return self.hasTileElement(TileElement.WALL_RIGHT) or self.getNeighbouredTile(1, 0).hasTileElement(TileElement.WALL_LEFT)
    
    # whether the tile has a ceiling, respecting the tile on top
    def hasCeiling(self):
        return self.hasTileElement(TileElement.CEILING) or self.getNeighbouredTile(0, 1).hasTileElement(TileElement.FLOOR)
        
    # whether the tile has a floor, respecting the tile on the bottom
    def hasFloor(self):
        return self.hasTileElement(TileElement.FLOOR) or self.getNeighbouredTile(0, -1).hasTileElement(TileElement.CEILING)
       
    # whether the player is about to move out of the tile to the left side
    def isInLeftEndZone(self):
        return self.player.positionInTile < TileModel.LEAVE_THRESHHOLD
        
    # whether the player is about to move out of the tile to the right side
    def isInRightEndZone(self):
        return self.player.positionInTile > 1 - TileModel.LEAVE_THRESHHOLD    
        
    def leavesTile(self, direction):
        newPos = self.player.positionInTile + direction
        return (newPos < 0) or (newPos > 1)
        
    def isInEndZone(self):
        return self.isInLeftEndZone() or self.isInRightEndZone()
        
    # public
    # adapt for constant speed
    def moveLeft(self):
        self.moveHandler.move(-Player.STEP_SIZE)
        
    # public
    # adapt for constant speed
    def moveRight(self):
        self.moveHandler.move(Player.STEP_SIZE)
        
    def movePlayer(self, position):
        self.movePlayerTo(self.player.tile_column, self.player.tile_row, self.player.carrier, position)
        
    def movePlayerTo(self, tileX, tileY, carrier, position):
        x = position
        if ((carrier & TileElement.CEILING) > 0):
            y = 1
        elif ((carrier & TileElement.FLOOR) > 0):
            y = 0
        elif ((carrier & TileElement.STAIRS_BL) > 0):
            y = x
        elif ((carrier & TileElement.STAIRS_BR) > 0):
            y = 1 - x
        self.player.carrier = carrier
        self.player.tile_column = tileX
        self.player.tile_row = tileY
        self.player.x = x * self.player.tile_width
        self.player.y = y * self.player.tile_height
        self.player.positionInTile = position
        self.player.move(0, 0, 0)
        
        
        

class Action:
    targetDirection = 0
    isUpsideDown = False
    blocks = False
    drops = False
    
    def __init__(self, targetDirection, isUpsideDown):
        self.targetDirection = targetDirection
        self.isUpsideDown = isUpsideDown
        
    def printAction(self):
        print 'action:', self.targetDirection, self.isUpsideDown, self.blocks, self.drops
    
    @staticmethod
    def block():
        result = Action(0, False)
        result.blocks = True
        return result
        
    @staticmethod
    def drop(upsideDown):
        result = Action(0, upsideDown)
        result.drops = True
        return result
        


class DirectionFilter:
    targetDirection = 0
    forbidden = 0
    
    def __init__(self, targetDirection, forbidden):
        self.targetDirection = targetDirection
        self.forbidden = forbidden
        
    def then(self, action):
        self.action = action
        return self
        
    def applies(self, star):
        return (star & self.targetDirection == self.targetDirection) and (star & self.forbidden == 0)
                
#   H   A   B
#    \  |  /        
# G --  ?  -- C      
#    /  |  \  
#   F   E   D     
class MoveHandler:
    
    def __init__(self, tileModel):
        self.initialize_filters()
        self.tileModel = tileModel

    def move(self, direction):
        if (not self.tileModel.leavesTile(direction)):
            self.tileModel.movePlayer(self.tileModel.player.positionInTile + direction)
            return
        isMirrored = direction < 0
        if (isMirrored):
            stairsF = TileElement.STAIRS_BR
            stairsH = TileElement.STAIRS_BL
        else:
            stairsF = TileElement.STAIRS_BL
            stairsH = TileElement.STAIRS_BR
        if (not self.tileModel.player.isUpsideDown):
            if (self.tileModel.player.carrier == stairsF): # F
                directionFilterList = self.FROM_F
                star = self.buildStar_bot(isMirrored)
            elif (self.tileModel.player.carrier == TileElement.CEILING or
                  self.tileModel.player.carrier == TileElement.FLOOR): # G
                directionFilterList = self.FROM_G
                star = self.buildStar_top(isMirrored)
            elif (self.tileModel.player.carrier == stairsH): # H
                directionFilterList = self.FROM_H
                star = self.buildStar_top(isMirrored)
        else:
            if (self.tileModel.player.carrier == stairsF): # F
                directionFilterList = self.FROM_F_UPSIDEDOWN
                star = self.buildStar_bot(isMirrored)
            elif (self.tileModel.player.carrier == TileElement.CEILING or
                  self.tileModel.player.carrier == TileElement.FLOOR): # G
                directionFilterList = self.FROM_G_UPSIDEDOWN
                star = self.buildStar_top(isMirrored)
            elif (self.tileModel.player.carrier == stairsH): # H
                directionFilterList = self.FROM_H_UPSIDEDOWN
                star = self.buildStar_top(isMirrored)
        print 'star:', star

        for directionFilter in directionFilterList:
            if (directionFilter.applies(star)):
                self.executeAction(directionFilter.action, direction, self.tileModel.player.carrier == stairsF, isMirrored)
                return
        print 'error, i\'m fucking stupid and don\'t know what I did - how unexpected!', star

    def executeBlock(self, direction):   
        if (direction < 0):
            pos = 0
        else:
            pos = 1
        self.tileModel.movePlayer(pos)

    def executeAction(self, action, direction, isInBottomTile, isMirrored): # we need to change tile
        action.printAction()
        if (action.blocks):
            self.executeBlock(direction)
            return
        if (action.drops):
            return
        if (isMirrored):
            stairs_B = TileElement.STAIRS_BR
            stairs_D = TileElement.STAIRS_BL
            diffX = -1
        else:
            stairs_B = TileElement.STAIRS_BL
            stairs_D = TileElement.STAIRS_BR
            diffX = 1
            
        # find target tile and carrier
        diffY = 0
        if (isInBottomTile):
            diffY = 1
            print 'in bottom liine'
        if (action.targetDirection == MoveHandler.B):
            targetOffsetX = diffX            
            targetOffsetY = diffY            
            print 'diff y:', targetOffsetY, 'dir:', action.targetDirection        
            carrier = stairs_B
        elif (action.targetDirection == MoveHandler.C):
            targetOffsetX = diffX            
            targetOffsetY = diffY    
            print 'diff y:', targetOffsetY, 'dir:', action.targetDirection        
            carrier = TileElement.FLOOR
        elif (action.targetDirection == MoveHandler.D):
            targetOffsetX = diffX            
            targetOffsetY = 1 - diffY
            print 'diff y:', targetOffsetY, 'dir:', action.targetDirection        
            carrier = stairs_D
        elif (action.targetDirection == MoveHandler.F):
            targetOffsetX = 0            
            targetOffsetY = 1 - diffY
            print 'diff y:', targetOffsetY, 'dir:', action.targetDirection        
            carrier = stairs_B
        elif (action.targetDirection == MoveHandler.G):
            targetOffsetX = 0            
            targetOffsetY = diffY            
            print 'diff y:', targetOffsetY, 'dir:', action.targetDirection        
            carrier = TileElement.FLOOR
        elif (action.targetDirection == MoveHandler.H):
            targetOffsetX = 0            
            targetOffsetY = diffY            
            print 'diff y:', targetOffsetY, 'dir:', action.targetDirection
            carrier = stairs_D            
        else:
            print 'omg this sucks bigtime'
            return
        # set player position
        
        tileX = self.tileModel.player.tile_column + targetOffsetX
        tileX = (tileX + self.tileModel.house.columns) % self.tileModel.house.columns
        tileY = self.tileModel.player.tile_row - targetOffsetY        
        pos = self.tileModel.player.positionInTile
        if (diffX == 0):
            if (direction < 0):
                position = - direction - pos
            else:
                position = pos - direction
        elif (direction < 0):
            position = 1 + direction - pos
        else:
            position = direction - 1 + pos
        self.tileModel.player.isUpsideDown = action.isUpsideDown
        if action.isUpsideDown:
            print 'OMFG WTF'
        self.tileModel.movePlayerTo(tileX, tileY, carrier, position)
        print 'new tile: ', tileY, tileX, 'with setting', self.tileModel.getTile(tileX, tileY).composition, carrier
        
    def buildStar_top(self, isMirrored):
        if (isMirrored):
            return self.buildMirroredStar_top();        
        else:
            return self.buildNormalStar_top();        
        
    def buildStar_bot(self, isMirrored):
        if (isMirrored):
            return self.buildMirroredStar_bot();        
        else:
            return self.buildNormalStar_bot();        
        
    def buildNormalStar_top(self):
        star = 0
        if (self.tileModel.hasRightWall()):
            star = star | MoveHandler.A
        if (self.tileModel.getNeighbouredTile(1, 0).hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.B
        if (self.tileModel.getNeighbouredTile(1, 0).hasFloor()):
            star = star | MoveHandler.C
        if (self.tileModel.getNeighbouredTile(1, -1).hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.D
        if (self.tileModel.getNeighbouredTile(0, -1).hasRightWall()):
            star = star | MoveHandler.E
        if (self.tileModel.getNeighbouredTile(0, -1).hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.F
        if (self.tileModel.hasFloor()):
            star = star | MoveHandler.G
        if (self.tileModel.hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.H

        return star
        
    def buildNormalStar_bot(self):
        print 'buildNormalStar_bot'
        star = 0
        if (self.tileModel.getNeighbouredTile(0, 1).hasRightWall()):
            star = star | MoveHandler.A
        if (self.tileModel.getNeighbouredTile(1, 1).hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.B
        if (self.tileModel.getNeighbouredTile(1, 1).hasFloor()):
            star = star | MoveHandler.C
        if (self.tileModel.getNeighbouredTile(1, 0).hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.D
        if (self.tileModel.hasRightWall()):
            star = star | MoveHandler.E
        if (self.tileModel.hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.F
        if (self.tileModel.hasCeiling()):
            star = star | MoveHandler.G
        if (self.tileModel.getNeighbouredTile(0, 1).hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.H
        return star

    def buildMirroredStar(self):
        if (self.tileModel.player.carrier == TileElement.STAIRS_BR):
            return buildStar_bot()            
        else:
            return buildStar_top()        
        
    def buildMirroredStar_top(self):
        star = 0
        if (self.tileModel.hasLeftWall()):
            star = star | MoveHandler.A
        if (self.tileModel.getNeighbouredTile(-1, 0).hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.B
        if (self.tileModel.getNeighbouredTile(-1, 0).hasFloor()):
            star = star | MoveHandler.C
        if (self.tileModel.getNeighbouredTile(-1, -1).hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.D
        if (self.tileModel.getNeighbouredTile(0, -1).hasLeftWall()):
            star = star | MoveHandler.E
        if (self.tileModel.getNeighbouredTile(0, -1).hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.F
        if (self.tileModel.hasFloor()):
            star = star | MoveHandler.G
        if (self.tileModel.hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.H
        return star
        
    def buildMirroredStar_bot(self):
        star = 0
        print 'buildMirroredStar_bot'
        if (self.tileModel.getNeighbouredTile(0, 1).hasLeftWall()):
            star = star | MoveHandler.A
        if (self.tileModel.getNeighbouredTile(-1, 1).hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.B
        if (self.tileModel.getNeighbouredTile(-1, 1).hasFloor()):
            star = star | MoveHandler.C
        if (self.tileModel.getNeighbouredTile(-1, 0).hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.D
        if (self.tileModel.hasLeftWall()):
            star = star | MoveHandler.E
        if (self.tileModel.hasTileElement(TileElement.STAIRS_BR)):
            star = star | MoveHandler.F
        if (self.tileModel.hasCeiling()):
            star = star | MoveHandler.G
        if (self.tileModel.getNeighbouredTile(0, 1).hasTileElement(TileElement.STAIRS_BL)):
            star = star | MoveHandler.H
        return star
        
        
    def initialize_filters(self):
        self.FROM_F = [
          DirectionFilter(MoveHandler.B, MoveHandler.A).then(Action(MoveHandler.B, False)),
          DirectionFilter(MoveHandler.C, MoveHandler.A).then(Action(MoveHandler.C, False)),
          DirectionFilter(MoveHandler.D, MoveHandler.A).then(Action(MoveHandler.D, False)),
          DirectionFilter(MoveHandler.G, 0).then(Action(MoveHandler.G, True)),
          DirectionFilter(MoveHandler.H, 0).then(Action(MoveHandler.H, True)),
          DirectionFilter(MoveHandler.E, 0).then(Action.drop(False)),
          DirectionFilter(MoveHandler.A, 0).then(Action.block()),
          DirectionFilter(MoveHandler.F, 0).then(Action(MoveHandler.F, True)),
        ]
        self.FROM_G = [ 
          DirectionFilter(MoveHandler.A, 0).then(Action.block()),
          DirectionFilter(MoveHandler.B, 0).then(Action(MoveHandler.B, False)),
          DirectionFilter(MoveHandler.C, 0).then(Action(MoveHandler.C, False)),
          DirectionFilter(MoveHandler.D, 0).then(Action(MoveHandler.D, False)),
          DirectionFilter(MoveHandler.H, 0).then(Action(MoveHandler.H, True)),
          DirectionFilter(MoveHandler.E, 0).then(Action.drop(False)),
          DirectionFilter(MoveHandler.F, 0).then(Action(MoveHandler.F, True)),
          DirectionFilter(MoveHandler.G, 0).then(Action(MoveHandler.G, True)),
        ]
        self.FROM_H = [ 
          DirectionFilter(MoveHandler.A, 0).then(Action.block()),
          DirectionFilter(MoveHandler.B, 0).then(Action(MoveHandler.B, False)),
          DirectionFilter(MoveHandler.C, 0).then(Action(MoveHandler.C, False)),
          DirectionFilter(MoveHandler.D, 0).then(Action(MoveHandler.D, False)),
          DirectionFilter(MoveHandler.F, 0).then(Action(MoveHandler.F, True)),
          DirectionFilter(MoveHandler.G, 0).then(Action(MoveHandler.G, True)),
          DirectionFilter(MoveHandler.E, 0).then(Action.drop(False)),
          DirectionFilter(MoveHandler.H, 0).then(Action(MoveHandler.H, True)),
        ]
        self.FROM_F_UPSIDEDOWN = [
          DirectionFilter(MoveHandler.E, 0).then(Action.block()),
          DirectionFilter(MoveHandler.D, 0).then(Action(MoveHandler.D, True)),
          DirectionFilter(MoveHandler.C, 0).then(Action(MoveHandler.C, True)),
          DirectionFilter(MoveHandler.B, 0).then(Action(MoveHandler.B, True)),
          DirectionFilter(MoveHandler.H, 0).then(Action(MoveHandler.H, False)),
          DirectionFilter(MoveHandler.G, 0).then(Action(MoveHandler.G, False)),
          DirectionFilter(MoveHandler.A, 0).then(Action.drop(True)),
          DirectionFilter(MoveHandler.F, 0).then(Action(MoveHandler.F, False)),
        ]
        self.FROM_G_UPSIDEDOWN = [
          DirectionFilter(MoveHandler.E, 0).then(Action.block()),
          DirectionFilter(MoveHandler.D, 0).then(Action(MoveHandler.D, True)),
          DirectionFilter(MoveHandler.C, 0).then(Action(MoveHandler.C, True)),
          DirectionFilter(MoveHandler.B, 0).then(Action(MoveHandler.B, True)),
          DirectionFilter(MoveHandler.H, 0).then(Action(MoveHandler.H, False)),
          DirectionFilter(MoveHandler.F, 0).then(Action(MoveHandler.F, False)),
          DirectionFilter(MoveHandler.A, 0).then(Action.drop(True)),
          DirectionFilter(MoveHandler.G, 0).then(Action(MoveHandler.G, False)),
        ]
        self.FROM_H_UPSIDEDOWN = [
          DirectionFilter(MoveHandler.E, 0).then(Action.block()),
          DirectionFilter(MoveHandler.D, 0).then(Action(MoveHandler.D, True)),
          DirectionFilter(MoveHandler.C, 0).then(Action(MoveHandler.C, True)),
          DirectionFilter(MoveHandler.B, 0).then(Action(MoveHandler.B, True)),
          DirectionFilter(MoveHandler.F, 0).then(Action(MoveHandler.F, False)),
          DirectionFilter(MoveHandler.G, 0).then(Action(MoveHandler.G, False)),
          DirectionFilter(MoveHandler.A, 0).then(Action.drop(True)),
          DirectionFilter(MoveHandler.H, 0).then(Action(MoveHandler.H, False)),
        ]
    


    A = 1 << 0
    B = 1 << 1
    C = 1 << 2
    D = 1 << 3
    E = 1 << 4
    F = 1 << 5
    G = 1 << 6
    H = 1 << 7

