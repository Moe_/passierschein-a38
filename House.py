import random
import math
from Tile import *
from Player import *
from Dialogs import *
from DialogScene import *
from string import *

def distance(x1, y1, x2, y2):
    dx = x1 - x2
    dy = y1 - y2
    return math.sqrt(dx * dx + dy * dy)
    
def clamp(val, min, max):
    if val < min:
        return min
    elif val > max:
        return max
    return val

class House:
    def __init__(self, scene, rows, columns):
        self.columns = columns
        self.rows = rows
        self.mikeDebugt = False
        if not self.mikeDebugt:
            self.TileArray = [[Tile(0, scene) for col in xrange(columns)] for row in xrange(rows)]
            distributionArray = [4, 1, 1, 1, 1, 2, 3, 4, 4, 4, 0, 0]
            i = 0
            for tileCounter in distributionArray:
                for counter in range(0, tileCounter):
                    x = int(random.random() * columns)
                    y = int(random.random() * rows)
                    self.TileArray[y][x] = Tile(i, scene)
                i += 1
            print(self.TileArray)
                
        else:
            self.TileArray = [[Tile(1, scene),Tile(1, scene),Tile(2, scene),Tile(2, scene)], [Tile(3, scene),Tile(3, scene),Tile(4, scene),Tile(4, scene)], [Tile(0, scene), Tile(5, scene), Tile(5, scene), Tile(6, scene)], [Tile(7, scene), Tile(8, scene), Tile(9, scene), Tile(0, scene)]]

        tile = self.TileArray[0][0].getSize()
        print('mytile', tile)

        rect = self.TileArray[0][0].getSize()
        self.tile_width = rect.width + Tile.TYLE_OFFSET
        self.tile_height = rect.height + Tile.TYLE_OFFSET

        self.persons = getListOfPersons()
        count = len(self.persons)
        i = 0
        while i < count:
            row = int(random.random() * len(self.TileArray))
            column = int(random.random() * len(self.TileArray[0]))
            if self.TileArray[row][column].addCounter(i):
                i = i + 1
        self.player = Player(scene, 2, 2, 15, 15, self.tile_width, self.tile_height)
        self.updatePositions()

    def updatePositions(self):
        for row in range(len(self.TileArray)):
            for column in range(len(self.TileArray[0])):
                self.TileArray[row][column].setPosition((column + 0.5) * self.tile_width, (len(self.TileArray) - (row + 0.5)) * self.tile_height)
                self.TileArray[row][column].loadData(self, self.player)

    def cycleRowRight(self, row):
        size = len(self.TileArray)
        last = self.TileArray[row][size - 1]
        i = size - 1
        while i > 0:
            self.TileArray[row][i] = self.TileArray[row][i - 1]
            i = i - 1
        self.TileArray[row][0] = last
        self.updatePositions()
        
    def cycleRowLeft(self, row):
        size = len(self.TileArray)
        last = self.TileArray[row][0]
        i = 0
        while i < size - 1:
            self.TileArray[row][i] = self.TileArray[row][i + 1]
            i = i + 1
        self.TileArray[row][size - 1] = last
        self.updatePositions()

    def cycleColumnUp(self, column):
        size = len(self.TileArray[0])
        last = self.TileArray[0][column]
        i = 0
        while i < size - 1:
            self.TileArray[i][column] = self.TileArray[i + 1][column]
            i = i + 1
        self.TileArray[size - 1][column] = last
        self.updatePositions()

    def cycleColumnDown(self, column):
        size = len(self.TileArray[0])
        last = self.TileArray[size-1][column]
        i = size - 1
        while i > 0:
            self.TileArray[i][column] = self.TileArray[i - 1][column]
            i = i - 1
        self.TileArray[0][column] = last
        self.updatePositions()

    def updatePlayer(self, dt, xDir, yDir):
        if xDir > 0:
            self.TileArray[self.player.tile_row][self.player.tile_column].model.moveRight()
        elif xDir < 0:
            self.TileArray[self.player.tile_row][self.player.tile_column].model.moveLeft() 

    def checkCounter(self):
        tile_column, tile_row, x, y = self.player.getFullPosition()
        tile_column = clamp(tile_column, 0, self.columns - 1)
        tile_row = len(self.TileArray) - 1 - clamp(tile_row, 0, self.rows - 1)
        counters = self.TileArray[tile_row][tile_column].getCounters()
        size = self.player.getSize()
        y -= size.height/2
        for i in range(len(counters)):
            cx, cy = counters[i].getPosition()
            csize = counters[i].getSize()
            cx += self.tile_width/2
            cy += csize.height/2
            if distance(x, y, cx, cy) < 20:
                id = counters[i].getId()
                print(rstrip(self.persons[id]), str(id), id)
                dialog = DialogScene(rstrip(self.persons[id]), str(id), self, 3)
                dialog_scene = cocos.scene.Scene(dialog)
                cocos.director.director.push(dialog_scene)
                

    def shiftByColor(self, color):
        tile_column, tile_row, x, y = self.player.getFullPosition()
        tile_column = clamp(tile_column, 0, self.columns - 1)
        tile_row = clamp(tile_row, 0, self.rows - 1)
        if color == DIRECTION_COLORS[0]:
            cycleRowRight(tile_row)
        elif color == DIRECTION_COLORS[1]:
            cycleRowLeft(tile_row)
        elif color == DIRECTION_COLORS[2]:
            cycleColumnUp(tile_column)
        elif color == DIRECTION_COLORS[3]:
            cycleColumnDown(tile_column)
