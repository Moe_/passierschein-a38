from cocos.sprite import Sprite

class Counter:
    def __init__(self, id, node, x, y, type):
        self.id = id
        self.type = type
        if type == 8:
            self.sprite = Sprite( 'graphics/counter-stairs-up.png' )
            y = (y - self.getSize().height)/2
        elif type == 9:
            self.sprite = Sprite( 'graphics/counter-stairs-down.png' )
            y = (y - self.getSize().height)/2
        else:
            self.sprite = Sprite( 'graphics/counter.png' )
            y = -(y - self.getSize().height)/2 + 4
        node.add(self.sprite)
        self.sprite.position = (x, y)
        
    def relocate(self, x, y):
        if type == 8 or type == 9:
            y = (y - self.getSize().height)/2
        else:
            y = -(y - self.getSize().height)/2 + 4
        self.sprite.position = (x, y)

    def getSize(self):
        return self.sprite.get_rect()

    def getPosition(self):
        return self.sprite.position
        
    def getId(self):
        return self.id