import random

MAX_NUM_STAGES = 5 

def randomInt(maximum):
    """
    Gibt Zahlen zwischen `0` und `maximum - 1` zurueck
    """
    return int(maximum*random.random())

def randomOption():
    """
    Generiert zufaellige Interger-Zahl zwischen 2 und 4
    """
    number = randomInt(5) 
    while number < 2:
            number = randomInt(5) 
    return number

def generateUniqueIndices(howMany, possibleAnswersLength):
    uIs = []
    for i in xrange(0,howMany):
        theIndex = randomInt(possibleAnswersLength)
        if not theIndex in uIs:
            uIs.append(theIndex)
    return uIs

def getListOfPersons():
    lOfPersons = []
    infile = open('questions.txt', 'r')
    for li in infile:
        if li.startswith('###'):
            lOfPersons.append(li[4:])
    infile.close()
    return lOfPersons

def readQuestionsFrom(who, stage):
    """
    Liest die Fragen aus der Textdatei `questions.txt`
    """
    q = readFromFile('questions.txt', who, stage)
    return [''.join(an) for an in q]

def readAnswersFrom(who, stage):
    """
    Liest die Antworten aus der Datei `answers.txt`
    """
    a = readFromFile('answers.txt', who, stage)
    theJoined = [''.join(an) for an in a]
    return theJoined 

def readFromFile(filename, who, stage):
    """
    Das Dateiformat ist: 
    '### <Charaktername>\n'
    '<stageNr> <frageOderAntwortText>\n'
    """
    infile = open(filename, 'r') 
    questionList = []
    personTag = '### ' + who + "\n"
    correctPerson = False
    
    theLines = infile.readlines()
    for li in theLines:
        if li == personTag:
            correctPerson = True
            continue
        if li.startswith('###') and li != personTag: 
            correctPerson = False
        elif correctPerson:
            questionList.append(li)
    
    questionList = compactifyMultilineItems(questionList, stage)
    return questionList

def compactifyMultilineItems(lines, stage):
    numberedLineIndices = []
    shortendQuestionList = []
    perceivedStage = 0

    for i in xrange(0,len(lines)):
        if lines[i][0].isdigit():
            perceivedStage = int(lines[i][0])
            lines[i] = lines[i][1:]
            if perceivedStage == stage:
                shortendQuestionList.append([lines[i]])
        else:
            if perceivedStage == stage:
                shortendQuestionList[len(shortendQuestionList)-1].append(lines[i])
        
    return shortendQuestionList 

def testCompactification():
    testsubjekt = ["1 abc", "bcd", "cde", "2 blabla", "dings"]
    print compactifyMultilineItems(testsubjekt)
    pathological = ["1 abc", "2 xyz"]
    print compactifyMultilineItems(pathological)

def selectQuestion(who, stage):
    questions = readQuestionsFrom(who, stage)
    theQuestionsIndex = randomInt(len(questions)-1)
    return questions[theQuestionsIndex]

def selectAnswer(who, stage, numAnswers):
    selectedAnswers = []
    possibleAnswers = readAnswersFrom(who, stage)
    uniqueIndices = generateUniqueIndices(numAnswers, len(possibleAnswers))
    for i in xrange(len(uniqueIndices)):
        selectedAnswers.append(possibleAnswers[uniqueIndices[i]])

    return selectedAnswers

class Dialog(object):
    """
    `who` ist die Person, mit der man den Dialog fuehrt (Datentyp String).
    Die Stages sortieren die Fragen und Antworten grob, damit wenigstens ein bisschen logische Abfolge gesichert ist.
    Die Stage wird beim Auswaehlen einer Antwort erhoeht. 
    
    Um einen Dialog mit Person <who> zu erzeugen, ein Dialog-Objekt spawnen:
        
        d = Dialog(<who>)

    """
    def __init__(self, who):
        self.charakter = who 
        self.stage = 1
        self.maximumStage = randomInt(MAX_NUM_STAGES)+1
        self.generateQuestionAndAnswers(self.stage)
    
    def generateQuestionAndAnswers(self, theStage):
        self.question = selectQuestion(self.charakter, theStage)
        self.numAnswers = randomOption()
        self.answerOptions = selectAnswer(self.charakter, theStage, self.numAnswers)
    
    def incStageAndNextQuestion(self):
        if self.stage < self.maximumStage:
            self.stage += 1
            self.generateQuestionAndAnswers(self.stage)
        else:
            self.stage = self.maximumStage

    def printDialog(self):
        """ 
        Zu Debuggingzwecken
        """
        print "Stages: ", self.maximumStage, "\n"
        print self.charakter+": "
        print self.question+"\n\n"

        for a in self.answerOptions:
            for s in a:
                print s

