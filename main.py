import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))
#


import cocos
from cocos.director import director
from cocos.actions import *
from cocos.layer import *
from cocos.sprite import Sprite
import pyglet

from GameScene import *
from constants import *

if __name__ == "__main__":
    # director init takes the same arguments as pyglet.window
    cocos.director.director.init(GAME_RES_WIDTH, GAME_RES_HEIGHT, "Passierschein A38")

    # We create a new layer, an instance of HelloWorld
    game_layer = GameScene ()

    # A scene that contains the layer hello_layer
    main_scene = cocos.scene.Scene (game_layer)

    # And now, start the application, starting with main_scene
    cocos.director.director.run (main_scene)

    # or you could have written, without so many comments:
    #      director.run( cocos.scene.Scene( HelloWorld() ) )
